### Analysing the problem

After googling "vanity number", I see two ways to interpret the objective, and in real life I would make sure I had clearly understood what the customer wanted. The phrase "convert into vanity numbers" may mean registering a new (more memorable) phone number to act as an alias to the original one, something that is easily possible in AWS Connect. However, I think the objective is meant to be interpreted as "convert subsequences of the original phone number into words, where "1" can become "A","B" or "C", and so on following the traditional printing of letters on phone keypads (which I naively hope is fairly standard). Reading the objective in this way feels more consistent.

This then leads me to ask, for the first time, the question of what a phone number can look like. I remember being told that the assumptions made by typical British people are not always true even in Britain, and this solution could be intended to be international. I will do my best to avoid hard-coded assumptions about things like the length of the number, or which correspond to an area code. If I cannot get my solution to be global within the allotted time, however, I will assume that the phone number is British, and look up the official standards for such a number. It would be more reasonable to assume that the number is American, but I need to be able to test this quickly. What I will first do, though, is look up the AWS Connect documentation to find out in what format it would pass a phone number to a Lambda function, since that is probably where my Lambda function will be getting its phone number string from. In particular, I need to know what characters it might contain, such as decorative hyphens, spaces, or the initial "+" from the country code. I googled "falsehoods programmers believe about phone numbers" to try to steer clear of common pitfalls. To my relief, most of those that I saw were related to actually calling a number, or using it as a unique identifier for a person/organisation. Neither of those are within my current scope.

I remember texting in the early 2000s, so programs to convert digit sequences to words have been around for a while, so it's time to scour the internet for previously invented wheels. In doing so, however, I must take care. The word dictionary, for example, should not contain obscene words or ethnic slurs. A dictionary developed for, say, linguistic or sociological research is therefore probably inappropriate here.

### finding a python module on Google.

I found a Python module called "vanitynumber" (https://pypi.org/project/vanitynumber/) that looks like an excellent place to start. It is released under an MIT license, so I should be able to use it here. I only hope it doesn't have the US phone number standard hardcoded. As I get stuck in, it becomes clear that there are some issues. There is a function to check that the number is a valid US phone number, so I will need to disable that. More concerning is the dictionary, which is far from family-friendly (but at least contains no ethnic slurs that I know of). I would be a fool to filter it myself, so I will see if I can obtain another by Friday. Get the thing working first, I guess. The module's documentation tells me that it ignores two letter words, since these cause unmemorable vanity numbers. This seems like a good idea. It also appears to leave the first 4 digits untouched, i.e. it prefers "1-855-PAINTER" to "BULLPAINTER". Maybe that's the convention for these things, and it's best not to mess with it. We'll see. I have never seen them in England, so perhaps I should look up the conventions.

At first glance, it seems hardcoded to US phone number standards/conventions (albeit with some evident intention to be extensible through a config file in the future). Following the trail of function calls, however, it turns out that I can just call the "find_words_from_numbers" function, which does not do any US phone number validation/hyphenation. It even seems to order the output according to the very definition of "niceness" that I am using, which is a happy coincidence. My only concern with it is that it outputs the final, unseparated string, e.g "34BIG98BEN4", which Polly can't be trusted to pronounce, and which won't look great in a web app. I may try to edit it to return something like ["34,"BIG,"98","BEN","4"] so I can format it myself.

### "Best" vanity words

My mind is spinning with possibilities. Whatever I do, I think I will have to be careful to ensure that the "niceness" of a vanity number is not too tightly coupled with the rest of the code, since we might want to change the definition later. For the sake of speed, I will probably go with "fewest digit characters" since it is easy to implement quickly. It turns out that the module has its own definition for sorting purposes, which it implements by defining a class with comparator methods, each of which uses complicated Boolean logic. I would love to tinker with it, but I probably won't. It's stated intention is "longest contiguous letter sequence", which is not quite the same as what my "niceness" function looks for, but often ends up returning lists in the very same order.

I have no intention of deleting/commenting out my niceness function, since it is good to have in case I one day think of a good definition. I note it here since it means that, in practice, my Lambda function will define the "nicest" vanity word to be "From the n strings with the longest chain of letters, select the one with the highest absolute number of letters" where n is the "max responses" parameter of the function that I am calling from the module. I set it to 20 since another function within the module uses that, and I don't know the running time implications for setting it too high; Connect will time out after 8 seconds.

I can think of examples as to why my definition is not great in the real world. "34RED9BULL4" is probably worse than some strings with fewer letters, since the letters in between words don't help. Maybe the module has the better definition

### The Contact Flow

I was expecting this to be far harder than it was since I have not directly played with Contact Flows for some time. With the help of the documentation, StackOverflow, and a blog or two, I was able to work out how to pass the caller's phone number to a Lambda function as event data. I started with a simple Lambda function that just printed the event dictionary so that I could be sure how to extract the phone number from it. Somewhat tricker was figuring out precisely what format the return value should be in so that I could reference it from the contact flow. I can't tell whether Amazon are bad at indexing their documentation or I am just bad at googling it, but the first page I find is always vague on details like that, and contains no links to anything more specific. I got frustrated, as can be seen from the following line that was in my function at one point:

```python
return {'speech':'please say something'}
```
fortunately, a blog post about a similar project (the Lambda function returned a random joke) showed me how to reference it (and contained a link to the actually helpful documentation page).

After registering a phone number, I was able to verify that it works. Indeed, it works far better than I had anticipated. Maybe I gave Polly too little credit. I still don't trust it to say things like "BODEWELL" correctly: I think it would probably say "BOD EWE LL" and confuse the listener, but I doubt I can fix that in the time frame since it means getting my hands very dirty messing with the module, which has some rather tight coupling in places. I don't think there is any one simple edit that will cause it to return a list of strings rather than a single mashed-together string in cases like that. I would love to do this given more time.

### Fun with CloudFormation

This may be a bonus task, but describing verbally how to set up a Lambda function and DynamoDB table click-by-click may take longer, and be harder to test without subjecting my wife/friends to a very boring evening. I naively thought to myself "I will get the template for the Contact Flow out of the way first since it is the one I am least familiar with". If you know much about this, you are probably laughing now. It turns out that there is a good reason why the task says "the contact flow can be imported manually", but I just had to learn the hard way. Amazon's documentation claims that they support this, but includes not a single example of what the "content" field of the template should actually contain. I know it has to be a string, not a JSON/YAML object. I tried escaping the JSON. I tried a strict string in YAML of non-escaped JSON. I tried an example of "flow language" JSON copied from their own documentation. I tried the exported contact flow (but exporting that in a format that could be used anywhere other than the console is clearly crazy talk as far as AWS are concerned). Nothing worked. It was the fun least part of all this.

Writing a template that set everything else up was pretty straightforward. If I had more time, I would learn SAM, since that would probably be better for something like this, but I chose to stick with CF simply because I knew I could get it done in time. One thing I don't like about CloudFormation for Lambda functions is the limitations for the "Code" attribute of the template. I am sorry for making you set up an S3 bucket, when this is more work than setting up a function with an empty code file, and uploading the zip archive to it directly. Still, it is probably easier than following a painful click-by-click guide for every component of this application.

### documentation

I wrote the README, trying to ensure that my deployment instructions were correct, unambiguous, and intelligible to an over-tired worker with basic experience of AWS. I hope I succeeded, but I didn't get time to rope in a test subject. I am afraid you will have to do it. I look forward to your constructive feedback.

Also, I should just about have time to whip up a quick architecture diagram before I submit this.

### Bad Practice confession

Some (not all) of my unit tests were written at the very end of the project. The functions where I did think to write tests early were probably better for it. In particular, there is a hard-coded '20' in the "getVanities" function that I don't have time to change now, and that would not have been there if I had written unit tests earlier. I didn't really use them during development. I relied instead on the Cloudwatch logs from the Lambda function, and calling the number that I had registered. I really need to test this with a very large number of phone numbers, which I certainly did not do. I had the luxury of a separate account in which to test all this, which enabled me to act as one would if one were "testing in prod" wihout risk of actually breaking anything. Also, I could have been more careful when writing IAM policies, since the Lambda function can, technically, publish logs to the CloudWatch Log groups of functions other than itself. I wish I could say that that was the worst IAM sin I have committed in my time.

### If I had more time

I could talk your ear off about this, I fear. In brief:

- change the dictionary to something with no rude words, and no obscure 3 letter words that are hard to pronounce
- do the "Super Bonus Task"
  - The table would need a "timestamp" field, and I might have to do somehthing clever with partition/sort keys to avoid having to scan the table every time someone used the app.
  - The contact flow would have to be expanded to include a "Consent" question, which would update a field in the table. GDPR and all that.
- pick a different voice for Polly
- do something clever with detecting country codes, and have a lookup table matching country codes to phone number standards
- think of a better standard of "niceness" for vanity numbers.
- tinker with the module to make it return something more useful downstream
  - If I manage that, edit  the "formatForPolly" function to add gaps between the words, so it is easier to hear them
  - build my definition of "niceness" into it's own sorting method, rather than layering it on top
  - I might at least send a pull request with some more unit tests for peripheral functions that looked like they would help me, but didn't do what they were meant to
- Learn SAM and give you a better deployment package
- Submit a support ticket to Amazon about their documentation for Contact Flow resources in CloudFormation
- write more unit tests
- make my functions more testable
- get someone other than me to write some additional unit tests
- Create a "production" alias for the Lambda function, and have the contact flow point to its ARN instead of the $LATEST version
- Continuous Integration

### Concluding remarks

Thanks for this project. It was engaging and educational. I really enjoyed it. I do hope my output was of sufficient quality that you will consider me for the job.

I learned more about Connect in particular, and reading the module's code caused me to take an interest in Trie and heap data structures. I intend to read more about these (I had encountered heaps before, but not used them in my own work). It also caused me to muse on subconscious assumptions when programming, and tightly coupled functions/classes. I hope my own work will improve as a result of this.

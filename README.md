# Acknowledgements

This project uses the "vanitynumber" python module (https://pypi.org/project/vanitynumber/), released under the MIT License.

# What this is

The repo contains everything you need to deploy a fun "vanity number" application to your AWS environment

```monospaced text
                                            Your AWS Account
                         ┌───────────────────────────────────────────────────────────────┐
                         │                                                               │
User ────────────────────┼────►    Telephone Endpoint                                    │
                         │   (you must set this up yourself)                             │
 ▲                       │                  │                                            │
 │                       │                  │                                            │
 │                       │                  │                                            │
 │                       │                  │                                            │
 │                       │                  ▼                                            │
 │           best three  │             Amazon Connect                                    │
 └──────────   words     ├──────────    Contact Flow                                     │
                         │                  │   ▲                                        │
                         │                  │   │                                        │
                         │                  │   │                                        │
                         │                  ▼   │                                        │
                         │                  /\                                           │
                         │                    \               best five       DynamoDB   │
                         │                   / \        ────►   words   ─────► Table     │
                         │                  /   \                                        │
                         │               Number-to-word                                  │
                         │                 Converter                                     │
                         │                                                               │
                         │                                                               │
                         │                                                               │
                         └───────────────────────────────────────────────────────────────┘
```

# Deployment Instructions

## the Lambda function, execution role, and DynamoDB table

- create a zip archive of the contents of the "function-code" directory
- upload this zip archive to an S3 bucket that is accessible from the account in which you use CloudFormation
- Run the template "CloudFormation.yml" in Cloudformation. You will need the bucket name and object key within the bucket from earlier

## The Contact Flow

This one involves a few manual steps, I am afraid

- When the CloudFormation stack is complete, look at the Outputs tab. You will find the Arn of the lambda function there. Copy it
- open "vanity-numbers-contact-flow.json" and look at line 19 where it says "value": "PASTE-FUNCTION-ARN-HERE". Replace the whole phrase between the double quote marks on the right hand side of the colon.
- go to Amazon Connect in the AWS console. If you do not have an existing instance that you wish to use, create one now.
- click on the alias (NOT the access URL just yet) of the instance that you wish to use. Click "Contact Flows" in the menu on the left.
- Scroll to "AWS Lambda" and select the function that was created earlier from the drop-down menu. click the "Add Lambda function" button, and confirm that the Arn column looks right. This will make the function accessible to contact flows in your Connect instance
    - if your organisation has more Lambda functions than the drop-down menu can support, you may need to go into CloudShell and do this via the API
- Now go to your instance with its access URL and log in with an account that has admin privileges. In the sidebar on the left, go to "Routing" -> "Contact Flows". Click the "Create contact flow" button on the top right
- click on the drop-down menu button next to the "Save" button at the top right. Choose "Import flow".
- In the box that pops up, click "Select" and navigate to git repo on your local machine. Select the "vanity-numbers-contact-flow.json" file, and click "Import". You should now have a working contact flow that needs only to be associated with a phone number to work.

# For the evaluator

Most of the files in the "function-code" directory are for a module that I imported (and its dependencies). The only file in that directory that I wrote is called "lambda_function.py". The files in the parent directory (including this README) are all my own work. The THOUGHT-PROCESS.md is, no doubt, of particular interest for the purpose of evaluating my suitability for the job

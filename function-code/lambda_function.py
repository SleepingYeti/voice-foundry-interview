import boto3
from vanitynumber import find_words_from_numbers
import os
import json

dynamodb = boto3.client('dynamodb')

def lambda_handler(event,context):

    rawNumString = event['Details']['Parameters']['callerNumber']

    # separate couontry code from other digits
    # I suspect I am making an assumption that effectively hard-codes it to UK standards. Sorry
    initialCode = rawNumString[0:-10]
    strippedNum = rawNumString[-10:]

    if not all(character.isdigit() for character in strippedNum) :
        raise ValueError
    vanities = [ initialCode + vanity for vanity in getVanities(strippedNum) ]
    bestFive = vanities[0:5]
    bestThree = vanities[0:3]

    tableName = os.environ['TableName'] # get DynamoDB table created by CloudFormation
    try:
        dynamodb.put_item(
            TableName = tableName,
            Item = {
                'phoneNumber':{'S':rawNumString},
                'Vanities':{'L':[{'S': i } for i in bestFive]}
            }
        )
    except Exception as err:
        "Could not update dynamodb table"
        print(err)

    return {'speech':formatForPolly(bestThree)}

def getVanities(numberString):
    output = find_words_from_numbers(numberString,20)
    # the next line, may not be necessary, as the module's own sorting might work in the exact same way
    output.sort(key=niceness,reverse=True) #descending order of niceness
    return output

def niceness(vanity) :
    # output must be a comparable object, such as an integer, since lists are sorted by it
    # fewer digit characters => nicer vanity number
    # very likely to want to change this
    digitCount = sum(character.isdigit() for character in vanity)
    return len(vanity) - digitCount

def formatForPolly(vanities) :
    beginPhrase = "Your best " + str(len(vanities)) + " vanity numbers are: "
    listPhrase = ' . The next one is: '.join(vanities)
    endPhrase = ". Goodbye"
    return beginPhrase + listPhrase + endPhrase

def test_niceness():
    assert(hasattr(niceness("h3llow0r1d"),"__lt__")) # niceness must be useful for sorting
    assert(niceness ('7892385924') < niceness('NICETHINGS'))

def test_formatForPolly():
    assert(formatForPolly(['12POODLE','FIRE8HAZARD']) == 'Your best 2 vanity numbers are: 12POODLE . The next one is: FIRE8HAZARD. Goodbye')

# not sure how to write a decent test for the "getVanities" function in the remaining time. That probably means the function itself needs work. Oh, well.
